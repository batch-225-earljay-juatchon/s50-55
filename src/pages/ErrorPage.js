import { Link } from "react-router-dom";


export default function PageNotFound() {
  return (
    <>
      <h3>Page Not Found</h3>
      <p>
        Go back to the <Link to="/">homepage</Link>
      </p>
    </>
  );
}