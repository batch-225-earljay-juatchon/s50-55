import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

//  Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <App />
  </>
);

// const user = {
//   firstName: 'Earl',
//   lastName: 'Juatchon'
// };

// function formatName(user) {

//   return user.firstName + ' ' +user.lastName;
// }

// const element = <h1> hello, {formatName(user)}</h1>

// root.render(element)


